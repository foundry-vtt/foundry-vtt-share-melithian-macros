[![Version: 1.0.0](https://img.shields.io/badge/Version-1.0.0-blue)](https://gitlab.com/foundry-vtt/foundry-vtt-share-melithian-macros) [![Foundry version: >=0.5.0](https://img.shields.io/badge/FoundryVTT-%3E%3D0.5.0-brightgreen)](http://foundryvtt.com/) [![FVTT DND5E version: >=0.8.0](https://img.shields.io/badge/FoundryVTT_DND5E-%3E%3D0.8.0-brightgreen)](https://gitlab.com/foundrynet/dnd5e)

# Foundry VTT Share - Melithian Macros

Um módulo contendo um compêndio de macros para uso geral nos seus jogos de D&D 5e no FoundryVTT. Cada macro no compêndio possui uma descrição sobre o que ele faz.

## Instalação
Na tela de SETUP do FoundryVTT, escolha a opção "Add-On Modules" e então clique em "Install Module" e coloque a seguinte URL no campo "Manifest URL":

```https://gitlab.com/foundry-vtt/foundry-vtt-share-melithian-macros/-/raw/master/module.json```
  
Alternativamente, você pode fazer o download do seguinte arquivo [melithian-macros.zip](https://gitlab.com/foundry-vtt/foundry-vtt-share-melithian-macros/-/jobs/artifacts/master/raw/melithian-macros.zip?job=build) e extraí-lo na pasta ```Data/modules```.
  
Após ter feito isso, você deve ativar o módulo nas configurações do mundo em que pretende usá-lo e então buscar o compêndio "Melithian - Macros" e importar os macros que deseja.